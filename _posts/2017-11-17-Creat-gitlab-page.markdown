---
layout: post
title:  "Creat gitlab page"
date:   2017-11-17
categories: xds update
---
## Gitlab网页制作

### Step 1 建立项目

建立Gitlab Page有两个方式，可以从fork一个历程开始，或者新建全新的项目。Gitlab Page可以完成静态网页。

### Step 2 配置Runner

使用Runner，Runner可以使用Gitlab提供的共享服务器，或者自己提供一个。

#### 共享Runner和私有Runner的区别：

-  共享Runner：**共享运行**程序对于具有相似要求的多个项目很有用。而不是让多个Runner一对一完成项目，可以有少数的处理多个项目的Runner。

- 私有Runner：一个人用的Runner，响应快，但是资源利用率不高。

  *可以在设置里配置不同的Runner，在设置--CI/CD里面选择即可*

#### 注册Runner

共享Runner不需要安装，私有Runner需要安装并且进行配置，不赘述。

#### 绑定Runner

- LINUX：

1. 运行以下命令：

   sudo gitlab-runner register 

2. 输入你的GitLab实例URL：

   Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com ) https://gitlab.com 

3. 输入您获得的注册Runner的令牌：

   Please enter the gitlab-ci token for this runner xxx 

4. 输入跑步者的描述，你可以稍后在GitLab的UI中进行更改：

   Please enter the gitlab-ci description for this runner [hostame] my-runner 

5. 输入[与Runner关联](https://docs.gitlab.com/ce/ci/runners/#using-tags)的[标签](https://docs.gitlab.com/ce/ci/runners/#using-tags)，稍后可以在GitLab的用户界面中进行更改：

   Please enter the gitlab-ci tags for this runner (comma separated): my-tag,another-tag 

6. 选择Runner是否应该选择没有[标签的](https://docs.gitlab.com/ce/ci/runners/#using-tags)作业，可以稍后在GitLab的UI中进行更改（默认为false）：

   Whether to run untagged jobs [true/false]: [false]: true 

7. 选择是否将Runner锁定到当前项目，稍后可以在GitLab的UI中进行更改。Runner特定时有用（默认为true）：

   Whether to lock Runner to current project [true/false]: [true]: true 

8. 输入[Runner执行者](http://docs.gitlab.com/runner/executors/README.html)：

   Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell: docker 

9. 如果您选择Docker作为您的执行程序，则会要求您为默认图像用于未定义的项目`.gitlab-ci.yml`：

   Please enter the Docker image (eg. ruby:2.1): alpine:latest 

- 另外的操作系统省略。

### Step 3 配置项目

如果跳过这一步，网站在**https：// username .gitlab.io /projectname上提供**，其中*username*是GitLab上我的用户名，*projectname*是在第一步中分配的项目的名称。

如果需要网站在**https：// xds95.gitlab.io上可用**，要在项目**设置**里配置，并将项目重命名为xds95.gitlab.io。

### Step 4 运行Pipeline并访问

手动运行pipeline导出工程。或者提交并推送一个更改。更改相应的页面，并生成CCS网页代码文件，就可以完成建立。并从任何一个可以上网的终端进行访问。



作业：[xds95.gitlab.io](xds95.gitlab.io)



