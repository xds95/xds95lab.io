---
layout: post
title:  "Readling lists for new EGOV@606 students"
date:   2017-11-17
categories: xds update
---
### Readling lists  for new EGOV@606 students[^7]

[^7]: 如果你看到的是打印版，请连上实验室网线之后，登录<xds95.gitlab.io>查看。

#### 必读书目[^3]：

[^3]: 必须学习，实验室的主要研究方向。

- [Deep Learning](http://www.deeplearningbook.org/), Yoshua Bengio, Ian Goodfellow, Aaron Courville, MIT Press, In preparation.[^1][中](https://github.com/exacity/deeplearningbook-chinese)[英](http://www.deeplearningbook.org/front_matter.pdf)

  [^1]: 这本书刚开始看会有难度，但绝对是经典中的经典，如果有不懂的地方，积极Google！看完必成大器！

- 机器学习. 周志华 [网盘](http://pan.baidu.com/s/1o7RlArS)[^6] 

[^6]: 密码：vq96(以后未提到密码的，以此为准)

- 统计学习方法. 李航 [网盘](http://pan.baidu.com/s/1o7RlArS) 

#### 参考书目：

- Pattern recognition. Bishop [PDF](http://users.isr.ist.utl.pt/~wurmd/Livros/school/Bishop%20-%20Pattern%20Recognition%20And%20Machine%20Learning%20-%20Springer%20%202006.pdf)

- 鸟哥的Linux私房菜. [^2]

  [^2]: 没找到PDF版本，不过实验室有，可相互借阅。Linux是实验室必学内容，但不必专项学习，在使用中熟悉即可。

- 数据挖掘导论 [网盘](https://github.com/exacity/deeplearningbook-chinese) 

- Reinforcement Learning: [An introduction. Richard Sutton and Andrew Barto.](http://incompleteideas.net/sutton/book/the-book-2nd.html) PDF and code

#### 软件学习：

- Python 推荐：[廖雪峰的Python教程](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000)
- Numpy 
- Scikit-Learning
- TensorFlow

#### 视频资源[^4]：
[^4]: 由难到易排序，内容可能有重复，可逐级学习。

- Machine Learning. [Coursera](https://www.coursera.org/learn/machine-learning)
- Deep learning. [网易云课堂](http://study.163.com/my#/smarts)
- Cs 231 Stanford 
- [deeplearning theory](stats385.github.io)

#### List of Lists[^5]:

[^5]: 更多的Reaning list作参考，开拓视野。

- http://deeplearning.net/reading-list/
- [Reading lists for new MILA students](https://docs.google.com/document/d/1IXF3h0RU5zz4ukmTrVKVotPQypChscNGf5k6E25HGvA/edit#)
- [ReadingOnDeepNetworks](http://www.iro.umontreal.ca/~lisa/twiki/bin/view.cgi/Public/ReadingOnDeepNetworks)